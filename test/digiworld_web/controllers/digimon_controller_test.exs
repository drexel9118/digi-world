defmodule DigiworldWeb.DigimonControllerTest do
  use DigiworldWeb.ConnCase

  alias Digiworld.World
  alias Digiworld.World.Digimon

  @create_attrs %{

  }
  @update_attrs %{

  }
  @invalid_attrs %{}

  def fixture(:digimon) do
    {:ok, digimon} = World.create_digimon(@create_attrs)
    digimon
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all digimons", %{conn: conn} do
      conn = get(conn, Routes.digimon_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create digimon" do
    test "renders digimon when data is valid", %{conn: conn} do
      conn = post(conn, Routes.digimon_path(conn, :create), digimon: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.digimon_path(conn, :show, id))

      assert %{
               "id" => id
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.digimon_path(conn, :create), digimon: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update digimon" do
    setup [:create_digimon]

    test "renders digimon when data is valid", %{conn: conn, digimon: %Digimon{id: id} = digimon} do
      conn = put(conn, Routes.digimon_path(conn, :update, digimon), digimon: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.digimon_path(conn, :show, id))

      assert %{
               "id" => id
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, digimon: digimon} do
      conn = put(conn, Routes.digimon_path(conn, :update, digimon), digimon: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete digimon" do
    setup [:create_digimon]

    test "deletes chosen digimon", %{conn: conn, digimon: digimon} do
      conn = delete(conn, Routes.digimon_path(conn, :delete, digimon))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.digimon_path(conn, :show, digimon))
      end
    end
  end

  defp create_digimon(_) do
    digimon = fixture(:digimon)
    %{digimon: digimon}
  end
end
