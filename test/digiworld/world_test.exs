defmodule Digiworld.WorldTest do
  use Digiworld.DataCase

  alias Digiworld.World

  describe "users" do
    alias Digiworld.World.User

    @valid_attrs %{age: 42, name: "some name"}
    @update_attrs %{age: 43, name: "some updated name"}
    @invalid_attrs %{age: nil, name: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> World.create_user()

      user
    end
    @tag :wip
    test "list_users/0 returns all users" do
      user = user_fixture()
      assert World.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert World.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = World.create_user(@valid_attrs)
      assert user.age == 42
      assert user.name == "some name"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = World.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = World.update_user(user, @update_attrs)
      assert user.age == 43
      assert user.name == "some updated name"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = World.update_user(user, @invalid_attrs)
      assert user == World.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = World.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> World.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = World.change_user(user)
    end
  end

  describe "digimons" do
    alias Digiworld.World.Digimon

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def digimon_fixture(attrs \\ %{}) do
      {:ok, digimon} =
        attrs
        |> Enum.into(@valid_attrs)
        |> World.create_digimon()

      digimon
    end

    test "list_digimons/0 returns all digimons" do
      digimon = digimon_fixture()
      assert World.list_digimons() == [digimon]
    end

    test "get_digimon!/1 returns the digimon with given id" do
      digimon = digimon_fixture()
      assert World.get_digimon!(digimon.id) == digimon
    end

    test "create_digimon/1 with valid data creates a digimon" do
      assert {:ok, %Digimon{} = digimon} = World.create_digimon(@valid_attrs)
    end

    test "create_digimon/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = World.create_digimon(@invalid_attrs)
    end

    test "update_digimon/2 with valid data updates the digimon" do
      digimon = digimon_fixture()
      assert {:ok, %Digimon{} = digimon} = World.update_digimon(digimon, @update_attrs)
    end

    test "update_digimon/2 with invalid data returns error changeset" do
      digimon = digimon_fixture()
      assert {:error, %Ecto.Changeset{}} = World.update_digimon(digimon, @invalid_attrs)
      assert digimon == World.get_digimon!(digimon.id)
    end

    test "delete_digimon/1 deletes the digimon" do
      digimon = digimon_fixture()
      assert {:ok, %Digimon{}} = World.delete_digimon(digimon)
      assert_raise Ecto.NoResultsError, fn -> World.get_digimon!(digimon.id) end
    end

    test "change_digimon/1 returns a digimon changeset" do
      digimon = digimon_fixture()
      assert %Ecto.Changeset{} = World.change_digimon(digimon)
    end
  end
end
