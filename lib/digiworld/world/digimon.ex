defmodule Digiworld.World.Digimon do
  use Ecto.Schema
  import Ecto.Changeset

  schema "digimons" do
    field :name, :string
    field :discription, :string
    field :level, :integer

    belongs_to :user, Digiworld.World.User
    timestamps()
  end

  @doc false
  def changeset(digimon, attrs) do
    digimon
    |> cast(attrs, [:name, :discription, :level, :user_id])
    |> validate_required([])
  end
end
