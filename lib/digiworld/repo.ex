defmodule Digiworld.Repo do
  use Ecto.Repo,
    otp_app: :digiworld,
    adapter: Ecto.Adapters.Postgres
end
