defmodule Digiworld.World do
  @moduledoc """
  The World context.
  """

  import Ecto.Query, warn: false
  alias Digiworld.Repo

  alias Digiworld.World.User

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    User
    |> join(:inner, [user], digimons in assoc(user, :digimons))
    |> preload([_, digimons], digimons: digimons)
    |> Repo.all()
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a user.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{data: %User{}}

  """
  def change_user(%User{} = user, attrs \\ %{}) do
    User.changeset(user, attrs)
  end

  alias Digiworld.World.Digimon

  @doc """
  Returns the list of digimons.

  ## Examples

      iex> list_digimons()
      [%Digimon{}, ...]

  """
  def list_digimons do
    Repo.all(Digimon)
  end

  @doc """
  Gets a single digimon.

  Raises `Ecto.NoResultsError` if the Digimon does not exist.

  ## Examples

      iex> get_digimon!(123)
      %Digimon{}

      iex> get_digimon!(456)
      ** (Ecto.NoResultsError)

  """
  def get_digimon!(id), do: Repo.get!(Digimon, id)

  @doc """
  Creates a digimon.

  ## Examples

      iex> create_digimon(%{field: value})
      {:ok, %Digimon{}}

      iex> create_digimon(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_digimon(attrs \\ %{}) do
    %Digimon{}
    |> Digimon.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a digimon.

  ## Examples

      iex> update_digimon(digimon, %{field: new_value})
      {:ok, %Digimon{}}

      iex> update_digimon(digimon, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_digimon(%Digimon{} = digimon, attrs) do
    digimon
    |> Digimon.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a digimon.

  ## Examples

      iex> delete_digimon(digimon)
      {:ok, %Digimon{}}

      iex> delete_digimon(digimon)
      {:error, %Ecto.Changeset{}}

  """
  def delete_digimon(%Digimon{} = digimon) do
    Repo.delete(digimon)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking digimon changes.

  ## Examples

      iex> change_digimon(digimon)
      %Ecto.Changeset{data: %Digimon{}}

  """
  def change_digimon(%Digimon{} = digimon, attrs \\ %{}) do
    Digimon.changeset(digimon, attrs)
  end
end
