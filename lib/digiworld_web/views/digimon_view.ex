defmodule DigiworldWeb.DigimonView do
  use DigiworldWeb, :view
  alias DigiworldWeb.DigimonView

  def render("index.json", %{digimons: digimons}) do
    %{data: render_many(digimons, DigimonView, "digimon.json")}
  end

  def render("show.json", %{digimon: digimon}) do
    %{data: render_one(digimon, DigimonView, "digimon.json")}
  end

  def render("digimon.json", %{digimon: digimon}) do
    %{id: digimon.id,
      name: digimon.name,
      discription: digimon.discription,
      level: digimon.level,
      user_id: digimon.user_id
  }
  end
end
