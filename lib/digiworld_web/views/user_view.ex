defmodule DigiworldWeb.UserView do
  use DigiworldWeb, :view
  alias DigiworldWeb.UserView
  alias DigiworldWeb.DigimonView

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    digimon = case Ecto.assoc_loaded?(user.digimons) do
      true -> render_many(user.digimons, DigimonView, "digimon.json")
      false -> []
    end
    

    %{id: user.id,
      name: user.name,
      age: user.age,
      digimons: digimon
    }
  end
end
