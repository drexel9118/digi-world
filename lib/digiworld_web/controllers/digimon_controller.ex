defmodule DigiworldWeb.DigimonController do
  use DigiworldWeb, :controller

  alias Digiworld.World
  alias Digiworld.World.Digimon

  action_fallback DigiworldWeb.FallbackController

  def index(conn, _params) do
    digimons = World.list_digimons()
    render(conn, "index.json", digimons: digimons)
  end

  def create(conn, %{"digimon" => digimon_params}) do
    with {:ok, %Digimon{} = digimon} <- World.create_digimon(digimon_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.digimon_path(conn, :show, digimon))
      |> render("show.json", digimon: digimon)
    end
  end

  def show(conn, %{"id" => id}) do
    digimon = World.get_digimon!(id)
    render(conn, "show.json", digimon: digimon)
  end

  def update(conn, %{"id" => id, "digimon" => digimon_params}) do
    digimon = World.get_digimon!(id)

    with {:ok, %Digimon{} = digimon} <- World.update_digimon(digimon, digimon_params) do
      render(conn, "show.json", digimon: digimon)
    end
  end

  def delete(conn, %{"id" => id}) do
    digimon = World.get_digimon!(id)

    with {:ok, %Digimon{}} <- World.delete_digimon(digimon) do
      send_resp(conn, :no_content, "")
    end
  end
end
