defmodule DigiworldWeb.PageController do
  use DigiworldWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
