defmodule Digiworld.Repo.Migrations.CreateDigimons do
  use Ecto.Migration

  def change do
    create table(:digimons) do
      add :user_id, references(:users)
      add :name, :string
      add :discription, :string
      add :level, :integer
      timestamps()
    end

  end
end
