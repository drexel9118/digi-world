alias Digiworld.Repo
alias Digiworld.World.User

Repo.insert! %User{
    name: "Ash",
    age: 22
}

Repo.insert! %User{
    name: "Rock",
    age: 18
}